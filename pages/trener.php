<?php
include "db.php";


$row = $db -> Prepare("SELECT * FROM `coach` WHERE `CoachId`=:id");
$row -> bindParam("id", $_SESSION['user_id'],PDO::PARAM_STR);
$row -> Execute();	
$row = $row -> fetch(PDO::FETCH_ASSOC);
//echo $row['NamePlayer'];

$pesel = $row['CoachPesel'] ;
 
$rok=substr($pesel, 0, 2);
$liczba=substr($pesel, 2, 2);
if($liczba<33){
	$wiek=1900;
 }elseif($liczba<53){
	$wiek=2100;
 } elseif($liczba<73){
	$wiek=2200;
 } elseif($liczba<93){
	$wiek=1800;
 }
 if(empty($pesel)){
 	$lata = 0;
 }else{
$lata = date('Y') - ($wiek + $rok);}
//echo 'Osoba ma: '.$lata.' lat';
?>



	<div class="col-md-2 ">
		
<?php

if(isSet($_GET['error']))
	{
		echo"<div class='alert alert-danger' role='alert'>";
		switch ($_GET['error']){
			
			case 21:
				echo "Błędny Pesel! ";
				break;
			case 23:
				echo "Błędny Pesel! ";
				break;	
			case 25:
				echo "Błędny Pesel! Rózne hasło z baza";
				break;
			default:
				echo "Błąd";
				break;
		}
		echo"</div>";
	}
		else if(isSet($_GET['success']))
	{
			echo"<div class='alert alert-success' role='alert'>";
		switch ($_GET['success']){
			
				default:
				echo "Dodano";
				break;
			
			
		}
		echo"</div>";
	}
	

?>
	</div>

<div class="col-md-8  " style="margin-top: 10px;" >
<form action="account1.php" method="post">
	<h4 class="text-center login-title" style="margin-top:0px;">Moje Dane</h4>
	<hr>
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i class="glyphicon glyphicon-user" > Imie: </i></span>
											<input class="form-control"type="text" name="coachname" id="coachname" placeholder="Imie" required <?php echo " value=\"$row[CoachName]\""; ?> />
										</div>
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i class="glyphicon glyphicon-user" > Nazwisko:</i></span>
											<input class="form-control" type="text" name="coachsurname" placeholder="Nazwisko"required <?php echo " value=\"$row[CoachSurname]\""; ?> />
										</div>
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i class="glyphicon glyphicon-asterisk" > Pesel:</i></span>
											<input class="form-control" type="text" name="CoachPesel" placeholder="Pesel" required  <?php echo " value=\"$row[CoachPesel]\""; ?>/>
										</div>	
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i> Wiek: </i></span>
											<input class="form-control" type="text" name="lata" placeholder="Lata" disabled <?php echo " value=\"$lata\""; ?>/>
										</div>	
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i  > Miejsce Zamieszkania:</i></span>
											<input class="form-control" type="text" name="CoachCity" placeholder="Miejsce Zamieszkania"  <?php echo " value=\"$row[CoachCity]\""; ?> />
										</div>
										<div class="text-center" style="padding-top:10px;">
											<input type= "submit"  class="btn btn-success" name="profilCoach" value="Zapisz" />	
										</div>
									
	</form>
	<hr>
	<form action="account1.php" method="post">
	<h4 class="text-center login-title" style="margin-top:0px;">O mnie </h4>

										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i > O mnie:</i></span>
											<textarea class="form-control" name="message_coach" placeholder="O mnie "></textarea>
											Informacje o mnie: <?php echo $row['OmnieCoach']; ?>
										</div>
										
										<div class="text-center" style="padding-top:10px;">
											<input type= "submit"  class="btn btn-success" name="omnieCoach" value="Zapisz" />	
										</div>
									
	</form>
	<hr>
						<h4 class="text-center login-title" style="margin-top:0px;">Kontakt</h4>

						<form action="account1.php" method="post" <hr>
						
										
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i>Kod pocztowy:</i></span>
									<input class="form-control" type="text" name="KontaktKodCoach" placeholder="np. 13-123 Rzeszów" <?php echo " value=\"$row[KontaktKodCoach]\""; ?> />
										</div>
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i>Numer domu:</i></span>
											<input class="form-control" type="text" name="KontaktAdresCoach" placeholder="Numer domu" <?php echo " value=\"$row[KontaktAdresCoach]\""; ?> />
										</div>
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i>Numer tel.</i></span>
											<input class="form-control" type="text" name="KontaktTel" placeholder="np.123-123-123" <?php echo " value=\"$row[KontaktTel]\""; ?> />
										</div>
								<hr>		
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i>Facebook:</i></span>
											<input class="form-control" type="text" name="face2" placeholder="Link do Facebooka" <?php echo " value=\"$row[face2]\""; ?> />
										</div>
										<div class="input-group" style="padding-top:10px;">
											<span class="input-group-addon "><i>Twitter:</i></span>
											<input class="form-control" type="text" name="twit2" placeholder="Link do Twittera" <?php echo " value=\"$row[twit2]\""; ?> />
										</div>

										<div class="text-center" style="padding-top:10px;">
											<input type= "submit"  class="btn btn-success" name="kontaktcoach" value="Zapisz" />	
										</div>
						</form>

										<hr>
										<h4 class="text-center login-title" style="margin-top:0px;">Dodaj Zdjęcie</h4>

						<form action="account1.php" method="post" files="true" enctype="multipart/form-data">		
										<div class="input-group">		
											<input class="form-control" type="file" name="img"/>
										</div>
										<div class="text-center" style="padding-top:10px;">
											<input type= "submit"  class="btn btn-success" name="zdjecieCoach" value="Zapisz" />	
										</div>
						</form>
								

						<hr>
							<h4 class="text-center login-title" style="margin-top:0px;">Moja Drużyna</h4>
						<form action="account1.php" method="post" files="true" enctype="multipart/form-data">		
							<div class="input-group">	
								<?php 	
									$st = "SELECT TeamId,TeamName FROM `team` WHERE `stat` = 0";
									$sth = $db -> query($st);
									$result = $sth ->fetch(PDO::FETCH_ASSOC); 
								?>
						
								<span class="input-group-addon">Moja Drużyna</span>
									<select name="druz1" class="form-control input-large"  >
										<?php
											while ($result) {
										?>
										<option value="<?php echo  $result['TeamName'] ?>"><?php echo  $result['TeamName'] ?> </option>
		
										<?php

										$result = $sth ->fetch(PDO::FETCH_ASSOC);
										}
										?>
									</select>
 
  							</div>
  						
										<div class="text-center" style="padding-top:10px;">
											<input type= "submit"  class="btn btn-success" name="druz_coach" value="Zapisz" />	
										</div>
						</form>
						


						


<!-- <form action="zapiszfota.php" method="post" enctype="multipart/form-data"  name="form1">
<p align="center">Wysyłanie plików na serwer. </p>
<table width="422" border="1" align="center" bordercolor="#0000FF" bgcolor="#C0C0C0">
  <tr>
    <th width="444" scope="row"><input name="plik" type="file" size="50"/>
      <input name="max_file_size" type="hidden" value="1048576" /></th>
  </tr>
  <tr>
    <th scope="row"><input value="Wyślij plik" type="submit" /> </th>
  </tr>
  </table>
								
		</form> -->







</div>

