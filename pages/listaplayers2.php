<?php
include 'db.php';

$row = $db -> Prepare("SELECT PlayerId FROM `players`");

$row -> Execute();	
$row = $row -> fetch(PDO::FETCH_ASSOC);


?>

<head>
 
     		<meta charset="utf-8"/>
	<meta http="X-UA-Comatibile" content = "IE=edge,chrome=1"/>
	<title> PZPN</title>	
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

    <script type="text/javascript">
    $(document).ready(function(){
    	    $("#txtSearch").on("keyup", function() {
    	//alert("asd");
			var value = $(this).val();

			$("#table2 tr").each(function(index) {
				if (index !== 0) {

					$(this).find("td").each(function () {
						var text = $(this).text().toLowerCase().trim();
						var not_found = (text.indexOf(value) == -1);
						$(this).closest('tr').toggle(!not_found);
						return not_found;
					});
				}
			});
		});

    });


		 function openDialog(id,id1)    {
        $('#cc').dialog({
            modal: true,
            open: function ()
            {
          
                $(this).empty().load('greeting.php?id='+id,'id1='+id1);
             
            },   
            buttons: {
        Ok: function() {
          $( this ).dialog( "close" );
        }
      },      
            height: 450,
            width: 900,
            title:"Wyslij Wiadomość"
        });
    }

    function team_profil(id)    {
        $('#prof').dialog({
            modal: true,
            open: function ()
            {
           
                $(this).empty().load('team_profil.php?id='+id);
                
            }, 
            buttons: {
        Ok: function() {
          $( this ).dialog( "close" );
        }
      },        
            height: 600,
            width: 1100,
            title:"Profil Użytkownika"
        });
    }

  function zapros(id,id1)    {
        $('#zapr').dialog({
            modal: true,
            open: function ()
            {

                $(this).empty().load('zapros.php?id='+id,'id1='+id1);
             
            },   
            buttons: {
        Ok: function() {
          $( this ).dialog( "close" );
        }
      },      
            height: 250,
            width: 300,
            title:"Zaproszenie"
        });
    }


  
    </script>
    <!-- --------------------------------------------------------------------------- -->


   
</head>

<body>
  


<div class= "col-md-2">
	
	
</div> 
<div id="zapr"></div>
<div id="cc"></div>
<div id="prof"></div>
<div id="contetntp" </div>
<div class="col-md-8 ">
<div  class="panel panel-primary">
	 	<div class="panel-heading">Wyszukiwarka:</div> 
	 		<div class="input-group" style="margin-top:10px;">
	 		<div class="input-group" style="padding-top:10px;">
				<span class="input-group-addon "><i class=" glyphicon glyphicon-search" ></i></span>
				<input class="form-control"type="text" name="haslo" id="txtSearch" style="color:black;" placeholder="Podaj szukane hasło"  />
			</div>

</div>
</div>
<div  class="panel panel-primary">

  <!-- Default panel contents -->
  <div class="panel-heading">Lista Zawodników</div>
 <!-- <div class="panel-body">
    <p>...</p>
  </div>-->

  <!-- Table -->
  <table id="table2" class="table">
    
  	<thead>
			<tr>
				

				<th >Imie</th>

				<th >Nazwisko</th>

				<th >Wiek</th>
				<th >Drużyna</th>

				<th >Profil </th>
				<th >Opcje</th>
				<th width="30px"> </th>
			
			</tr>
		</thead>
		<tbody>
		
		
			<?php 





$st = "SELECT * FROM `players`";
$sth = $db -> query($st);
$result = $sth ->fetch(PDO::FETCH_ASSOC); 




while ($result) {
	$pesel = $result['pesel_player'] ;
 
$rok=substr($pesel, 0, 2);
$liczba=substr($pesel, 2, 2);
if($liczba<33){
	$wiek=1900;
 }elseif($liczba<53){
	$wiek=2100;
 } elseif($liczba<73){
	$wiek=2200;
 } elseif($liczba<93){
	$wiek=1800;
 }
 if(empty($pesel)){
 	$lata = 0;
 }else{
$lata = date('Y') - ($wiek + $rok);}
//echo 'Osoba ma: '.$lata.' lat';
	?>

					<tr >
				
					
						<td><?php echo  $result['NamePlayer'] ?></td>
						<td><?php echo  $result['SurnamePlayer'] ?></td>
						<td><?php echo  $lata?></td>
						<td><?php echo  $result['MyTeam'] ?></td>
						
						<td><button class="btn btn-default" type="button" style="border:0;"  onclick="team_profil(<?php echo $result['PlayerId']; ?>)">
								<i class="glyphicon glyphicon-eye-open"></i></button>
						</td>
						<td>
						
						<div class="dropdown ">
							<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="border:0;">
								<i class="glyphicon glyphicon-option-vertical"></i>
								<span class="caret"></span></button>
								<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
									<li role="presentation"><a role="menuitem" tabindex="-1"  href="#"  onclick="openDialog(<?php echo $result['PlayerId']; ?> , <?php echo $_SESSION['user_id']?> )";>Wyślij Wiadomość</a></li>
								
									<li role="presentation"><a role="menuitem" tabindex="-1"  href="#"  onclick="zapros(<?php echo $result['PlayerId']; ?> , <?php echo $_SESSION['user_id']?> )";>Zaproś do Drużyny</a></li>
									
								</ul>
								</p>
							</div>
						</td>
					</tr>
						
		
		
	<?php

	$result = $sth ->fetch(PDO::FETCH_ASSOC);

}

				?>

			</tbody>

<?php

if(isSet($_GET['error']))
	{
		echo"<div class='alert alert-danger' role='alert'>";
		switch ($_GET['error']){
			case 41:
				echo "Wiadomośc nie wysłono ";
				break;
			case 40:
				echo "Nie podano żadnego hasła do wyszukiwarki!";
				break;
			
			case 35:
				echo "Błędny numer tel!";
				break;
			default:
				echo "Błąd";
				break;
		}
		echo"</div>";
	}
		else if(isSet($_GET['success']))
	{
			echo"<div class='alert alert-success' role='alert'>";
		switch ($_GET['success']){
			case 36:
				echo "Wiadomość Wysłano";
				break;
			case 40:
				echo "aaa";
				break;	
			
			
		}
		echo"</div>";
	}
	

?>
  </table>


<!-- <div id="zapytanie" title="Zaproszenie">
  Chcesz wysłać zaproszenie na testy??
 	<form action="account1.php">
 	<input class="form-control" type="text" name="lata" placeholder="Lata" disabled <?php echo " value=\"$row[PlayerId]\""; ?>/>

   		<div class="text-center" style="padding-top:10px;">
			<input type= "submit"  class="btn btn-warning" name="zapros" value="Zaproś" />	
		</div>
	</form>
  </div>
 -->
		
	
	</div>
	</div>



</body>