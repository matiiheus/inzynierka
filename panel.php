<?php

include "db.php";
?>
<!DOCTYPE HTML>
<html lang="pl">
	<head>
		<meta charset="utf-8"/>
		<title>PZPN</title>

				<!-- Skrypty i linki do bootstrap oraz jquery -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		




		<!--  ---------------------------------------------------------------->
		<body>
			 <?php

			 $user= $db -> Prepare('SELECT `login` FROM `users` WHERE `id` = :id');
				$user-> bindParam(':id',$_SESSION['user_id'], PDO::PARAM_STR);
				$user -> Execute(); 
				$user = $user->fetch(PDO::FETCH_ASSOC);
				//echo $user['login'];
			 ?>
			 <!-- menu główne -->
			
	<div class="conteiner">
  		<div class="col-xs-12">

			
			<?php 

			

				$id_user= $db -> Prepare('SELECT `role` FROM `status` WHERE `id_user` = :id');
				$id_user-> bindParam(':id',$_SESSION['user_id'], PDO::PARAM_INT);
				$id_user -> Execute(); 
				$id_user = $id_user->fetch(PDO::FETCH_ASSOC);

				

				if(isSet($_GET['success']) && $_GET['success'] == 4){
					echo "<div class='alert alert-success' role='alert'>Zalogowano</div>";
				}

				if($id_user['role']==0) {
					$menu = require_once "menu/menu.php";
				}
				elseif($id_user['role']==1) {
					$menu = require_once "menu/menu1.php";
				}
				elseif ($id_user['role']==2) {
					$menu = require_once "menu/menu2.php";
					 					
				}
				elseif ($id_user['role']==3) {
					$menu = require_once "menu/menu3.php";
					 					
				}
			?>
				
			
				 

				
			</div>

			<div class="col-xs-12">

				<?php
					if(isSet($_GET['page'])){
						require_once 'pages/' . $_GET['page']. ".php";

					}else{
						require_once 'pages/' . 'dashboard.php';
					}

				?>
			</div> 

</div>
		</body>
</html>