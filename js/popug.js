function ustawCookie(nazwa, wartosc, dni) {
    if (dni) {
        var data = new Date();
        data.setTime(data.getTime()+(dni*24*60*60*1000));
        var expires = "; expires="+data.toGMTString();
    } else {
        var expires = "";
    }
    document.cookie = nazwa+"=" + wartosc + expires + "; path=/";
}
 
// wyswietlanie popupa
  
$(document).ready(function () {
  
    // gdy popup nie byl jeszcze wyswietlony
 
    if (document.cookie.indexOf("popup") < 0) {
  
        // wyswietl go po 15 sekundach
        setTimeout(function () {
            $('#popup').modal('show');
        }, 15000); // milisekundy
  
        // lub wyswietl przy wychodzeniu ze strony
 
        $(document).mousemove(function (e) {
  
            if (e.pageY <= 5) {
                $('#popup').modal('show');
            }
  
        });
  
        // wyswietl ponownie po 3 dniach
 
       
  
    }
  
});