
<?php 

session_save_path("session/");
session_start();

include "db.php";

if(isSet($_POST['log_in'])){
	$login = $_POST['login'];
	$haslo = md5($_POST['haslo']);

	$user1 = $db -> Prepare('SELECT * FROM `users` WHERE `login` = :login');
	$user1 -> bindParam(":login",$login ,PDO::PARAM_STR);
	$user1 -> Execute();	
	$user1 = $user1 -> fetch(PDO::FETCH_ASSOC);
	
	//uzytkownik nie istnieje
	if(empty($user1)){
		header("Location: index.php?error=4");
		exit;
	}
	//sprawdzanie hasła z bazą 
	if($haslo != $user1['haslo']){
		header("Location: index.php?error=5");
		exit;
	}
	
//-------Aktywacja konta-------
	if($user1['klucz'] != "active"){
		header("Location: index.php?error=6");
		exit;
	}
//-----------------------------


	$_SESSION['user_id'] = $user1['id'];

	header("Location: index.php?success=4");
	
	exit;

	
}

//-------------------------------------odzyskiwanie hasła ------------------------------------------
	if(isset($_POST['reset_password'])){
		if(!isset($_POST['login']) || empty($_POST['login'])){
			header("Location: index.php?error=4");
			exit;
		}
					
					
					

		$user = $_POST['login'];
		$access_token = md5(mt_rand(0,50000));

		$reset = $db ->Prepare("UPDATE `users` SET `access_token` = :access_token WHERE `login` LIKE :login");
		$reset -> bindParam(':access_token',$access_token,PDO::PARAM_STR);
		$reset -> bindParam(':login',$user , PDO::PARAM_STR);
		$reset -> Execute();


		$email_template = "reset_tresc_poczty.html";
	$wiadomosc = file_get_contents($email_template);
	$wiadomosc = str_replace("[login]", $user, $wiadomosc);
	$wiadomosc = str_replace("[key]", $access_token, $wiadomosc);
	$wiadomosc = str_replace("[url]", "http://" . $_SERVER['HTTP_HOST'] . '/reset_password.php' , $wiadomosc);
	$naglowki = 'From : reset@pzpn.bluenet.net.pl'."\r\n".
				'Reply-TO: reset@pzpn.bluenet.net.pl'."\r\n".
				'Content-type: text/html; charset=utf-8'."\r\n";
	mail($user,"Reset Hasła " . $user, $wiadomosc, $naglowki);
	header("Location: index.php?success=5");
	}
	


//-----------------Wylogowanie ------------

if(isSet($_GET['logout'])){
	session_destroy();
	header("Location: index.php?success=3");
	exit;
}

//------------------zprawdzanie klucza przy recesie hasła z maila ----------------------------------
if(isset($_POST['change_password'])){
	$key = $_POST['access_token'];
	$reset= $db -> Prepare('SELECT `access_token` FROM `users` WHERE `access_token` = :access_token AND `login` =:login');
	$reset -> bindParam(':access_token',$key,PDO::PARAM_STR);
	$reset -> bindParam(':login',$_POST['login'], PDO::PARAM_STR);
	$reset -> Execute();

	if($reset -> rowCount() > 0 ){
		if($_POST['haslo'] != $_POST["haslo2"]){
			header('Location: reset_password.php?error=1&access_token='.$key);
		exit;
		}
		$reset = $db -> Prepare('UPDATE `users` SET `haslo` = :haslo  WHERE `login` =:login');
		$reset -> bindParam(':haslo',md5($_POST['haslo']), PDO::PARAM_STR);
		$reset -> bindParam(':login',$_POST['login'], PDO::PARAM_STR);
		$reset -> Execute();
		header('Location: index.php?success=6');
		exit;
	}
	header('Location: index.php?error=4');
	exit;

}
//------------------------------------sprawdzanie klucza przy kliknieciu w meilu 
if(isSet ($_GET['activate'])){
	$key = $_GET['activate'];
	$user1= $db -> Prepare('SELECT `klucz` FROM `users` WHERE `klucz`= :klucz');
	$user1 -> bindParam(":klucz",$key, PDO::PARAM_STR);
	$user1 -> Execute();
	
	if(empty($user1)){
		header("Location: index.php?error=3");
		exit;
	}
	$user1 = $user1 -> fetch(PDO::FETCH_ASSOC);
		if($user['klucz'] == "active"){
			header("Location: index.php?success=1");
			exit;
		}

//---------------------------------------zmana klucza na active-----------------------------
	$db -> Exec('UPDATE `users` SET `klucz` = "active" WHERE `klucz` = "'.$key.'"');
		header("Location: index.php?success=2");
		exit;
}
//-----------------------------------------------------------register-----------------------------------------------------------------------
if (isSet($_POST['register'])) {

	
	if(empty($_POST['login']) && empty($_POST['haslo']) && empty($_POST['haslo2'])){
		header("Location: index.php?error=0");
		exit;
	} 
	$status = $_POST['statusAdd'];
	$login = $_POST['login'];
	$haslo = md5($_POST['haslo']);
	$haslo2 = md5($_POST['haslo2']);
	$klucz = md5(mt_rand());
	//$id_status = (mt_rand(0,1000));
//sprawdzanie poprawnosci obu haseł ------------------------------------------
	if($haslo != $haslo2){
		header("Location:index.php?error=1");
		exit;
	}
//sprawdza login z baza -----------------------------------------------------------
	$user = $db->Prepare('SELECT `login` FROM `users` WHERE `login` = :login');
	$user -> bindParam(':login',$login, PDO::PARAM_STR);
	$user -> Execute(); 
	$user = $user->fetchALL();
	if(!empty($user)){
		header("Location: index.php?error=2");
		exit;
	}
//-------------dodawanie do bazy -------------------------------------------
	$db -> Exec('INSERT INTO `users`  VALUES ("","'. $login .'","'. $haslo .'","'. $klucz .'","")');

	$id_user= $db -> Prepare('SELECT `id` FROM `users` WHERE `login` = :login');
	$id_user-> bindParam(':login',$login, PDO::PARAM_STR);
	$id_user -> Execute(); 
	$id_user = $id_user->fetch(PDO::FETCH_ASSOC);
	$db -> Exec('INSERT INTO `status` VALUES ("","'.$id_user['id'].'","'.$status.'")');
	//$db -> Exec('INSERT INTO `wiadomosci` VALUES ("","'.$id_user['id'].'","")');
	$stat=1;
	if($status==1){
		$db -> Exec('INSERT INTO `team` VALUES ("'.$id_user['id'].'","","","","","","","","","","","","","","'. $stat .'")');
		
	} 

	if ($status==2) {
		$db -> Exec('INSERT INTO `players` VALUES ("","'.$id_user['id'].'","","","","","","","","","","","","","","","","","")');
		$db -> Exec('INSERT INTO `statystyki` VALUES ("","'.$id_user['id'].'","0","0","0","0","0","0")');
		
	}

if ($status==3) {
		$db -> Exec('INSERT INTO `coach` VALUES ("","'.$id_user['id'].'","","","","","","","","","","","","","")');
		
	}
	
//------------wysłanie meila z aktywacja ---------------------------------
	$email_template = "tresc_poczty.html";
	$wiadomosc = file_get_contents($email_template);
	$wiadomosc = str_replace("[login]", $login, $wiadomosc);
	$wiadomosc = str_replace("[key]", $klucz, $wiadomosc);
	$wiadomosc = str_replace("[url]", "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] , $wiadomosc);
	$naglowki = 'From : aktywacja@pzpn.bluenet.net.pl'."\r\n".
				'Reply-TO: aktywacja@pzpn.bluenet.net.pl'."\r\n".
				'Content-type: text/html; charset=utf-8'."\r\n";
	mail($login,"Aktywacja Konta " . $login, $wiadomosc, $naglowki);

	header("Location: index.php?success=0&email= ". $login);
	exit;

	
}
?>