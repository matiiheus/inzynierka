<?php 
	include "db.php";
$row = $db -> Prepare("SELECT * FROM `statystyki` WHERE `PlayerId`=:id");
$row -> bindParam("id", $_GET['id'],PDO::PARAM_STR);
$row -> Execute();	
$row = $row -> fetch(PDO::FETCH_ASSOC);
?>
<body>
 <label>Dynamika:</label>
<div class="progress">
  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" 
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($row['Dynamika']);?>%">
    <?php echo ($row['Dynamika']);?>%
  </div>
</div>
 <label>Wytrzymałość:</label>
<div class="progress">
  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" 
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($row['Wytrzymalosc']);?>%">
    <?php echo ($row['Wytrzymalosc']);?>%
  </div>
  </div>
   <label>Precyzja:</label>
<div class="progress">
  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" 
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($row['Precyzja']);?>%">
    <?php echo ($row['Precyzja']);?>%
  </div>
  </div>
   <label>Ambicja:</label>
<div class="progress">
  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" 
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($row['Ambicja']);?>%">
    <?php echo ($row['Ambicja']);?>%
  </div>
  </div>
   <label>Skuteczność:</label>
<div class="progress">
  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" 
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($row['Skutecznsc']);?>%">
    <?php echo ($row['Skutecznsc']);?>%
  </div>
  </div>
   <label>Mądrość Taktyczna:</label>
<div class="progress">
  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" 
  aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($row['Taktyka']);?>%">
    <?php echo ($row['Taktyka']);?>%
  </div>
  </div>