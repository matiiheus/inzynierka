
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title> PZPN</title>
			<!-- Skrypty i linki do bootstrap oraz jquery -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

		<!--  -->
</head>
<body>
<div class="row">
  <div class="col-sm-12">
    <div id="templatemo_copyright_wrapper">
		<div id="templatemo_copyright">
			<strong>PZPN</strong>
		</div> 
	</div>
    <div class="row">

	<div class="col-md-4 col-md-offset-4" style="margin-top: 10px;">
		<div class="login-form-primary">
			<div class="login-content " >
				
					<h5 class="text-center login-title" style="margin-top:0px;">Logowanie do systemu</h5>

					<!-- logowanie -->

							<form action="login.php" method="post">

								 	<div class="input-group" style="padding-top:10px;">
										<span class="input-group-addon "><i class="glyphicon glyphicon-user" ></i></span>
										<input type= "email" name="login" class="form-control" placeholder = "E-mail" />
									</div>
									<div class="input-group" style="padding-top:10px;">
										<span class="input-group-addon "><i class="glyphicon glyphicon-lock" ></i></span>
										<input class="form-control" type= "password" name="haslo" placeholder = "Hasło" />
									</div>
									
								
									<div class="text-center" style="padding-top:10px;">
								
										<input type= "submit" class="btn btn-success" value="Zaloguj" name="log_in"/>
										<input type= "submit" class="btn btn-success" value="Zapomniałes hasła?" name="reset_password"/>
									</div>


							</form>
							
					<!-- rejestracja -->
					<h5 class="text-center login-title" style="margin-top:10px;">Rejestracja do systemu</h5>
							
							<form action="login.php" method="post">
								<div class="input-group" style="padding-top:10px;">
									<span class="input-group-addon "><i class="glyphicon glyphicon-user" ></i></span>
									<input class="form-control" type= "email" name="login" placeholder = "E-mail" required />
								</div>
								<div class="input-group" style="padding-top:10px;">
									<span class="input-group-addon "><i class="glyphicon glyphicon-lock" ></i></span>
									<input class="form-control" type= "password" name="haslo" placeholder = "Hasło" required />
								</div>
								<div class="input-group" style="padding-top:10px;">
									<span class="input-group-addon "><i class="glyphicon glyphicon-lock" ></i></span>
									<input class="form-control" type= "password" name="haslo2" placeholder = "Powtórz Hasło"required  />
								</div>	
								<div class="input-group" style="margin-top:10px;">
									<span class="input-group-addon">Typ użtykownika</span>
										<select name="statusAdd" class="form-control input-large">
											<option value="1">Kierownik</option>
											<option value="2">Zawodnik</option>
											<option value="3">Trener</option>
										</select>
								</div> 
								 
								<div class="text-center" style="padding-top:10px;">
									<input type= "submit"  class="btn btn-success" value="Zarejestruj " name="register"/>		
								</div>	
									
									

							</form>
				
			</div>


					<!-- komunikaty -------------------------------------------------------------------------------- -->

					<?php


					if(isSet($_GET['error']))
					{
						echo"<div class='alert alert-danger' role='alert'>";
						switch ($_GET['error']){
							case 0:
								echo "Proszę wypełnić wszystkie wymagane pola";
								break;
							case 1:
								echo "Podane hasła różnią się od siebie";
								break;
							case 2:
								echo "Podany emeil istnieje w bazie";
								break;	
							case 3:
								echo "Wylogowano";
								break;
							case 4:
								echo "Nie ma takiego loginu";
								break;
							case 5:
								echo "Błędne Hasło";
								break;	
							case 6:
								echo "Brak akrywacji konta";
								break;	
							case 7:
								echo "Niepoprawny klucz resetu";
								break;		
							default:
								echo"Nieznany błąd spróbuj ponownie";
								break;
							
						}
						echo"</div>";
					}
						else if(isSet($_GET['success']))
					{
						echo"<div class='alert alert-success' role='alert'>";
						switch ($_GET['success']){
							case 0:
								echo "Pomyślnie zarejestrować konto. Proszę sprawdzić pocztę" . $_GET['email'];
								break;
							case 1:
								echo "Aktywowano konto" . $_GET['email'];
								break;	
							case 3:
								echo "Pomyślnie wylogowano";
								break;	
							case 4:
								echo "Pomyślnie zalogowano";
								break;
							case 5:
								echo "Pomyślnie wysłano reset hasła na twoją pocztę " . $_GET['email'];
								break;		
							default:
								echo"Powodzenie";
								break;
							
						}
						echo"</div>";
					}

					?>
					
				</div>
				
	

      
     

		
  </div>
  </div>
    <div id="templatemo_copyright_wrapper">
		<div id="templatemo_copyright">
			<strong>Copyright © 2015  Rzeszów | All Rights Reserved. | <span id="result_box" lang="en" xml:lang="en">It was designed</span> <a href="matiheus@o2.pl"  </a> matiheus@o2.pl</strong>
		</div> 
	</div>
			</div>
</div>


</body>
</html>
